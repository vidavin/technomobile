package com.example.myapplication

data class Meme(var nom : String, var urlImage : String) {

    private var id: String ?= null
    private var name: String ?= null
    private var url: String ?= null
    private var width: Int ?= null
    private var height: Int ?= null
    private var boxCount: Int ?= null
    private var captions: Int ?= null

}