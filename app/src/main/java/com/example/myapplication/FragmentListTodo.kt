package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentListTodo.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentListTodo : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_todo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView?.layoutManager = LinearLayoutManager(activity)

        /*var liste = arrayListOf("Coin", "Coin1", "Coin2", "Coin4", "Coin5", "Coin3", "Coin6", "Coin7", "Coin42")
        var adapter = CustomAdapter(liste)

        recyclerView?.adapter = adapter

        var buttonTodo = view.findViewById<Button>(R.id.button_add) */

        startNetworkCall()


        buttonTodo.setOnClickListener {
            liste.add("coin23")
            // recyclerView?.adapter = adapter
            adapter?.notifyDataSetChanged()
        }
    }

    private fun startNetworkCall()
    {
        CoroutineScope(Dispatchers.IO).launch {
            // Appel avec l’API
            val client = HttpClient(CIO)
            {
                install(ContentNegotiation) {
                    json(Json {
                        PrettyPrint = true
                        isLenient = true
                    })
                }
            }

            val response: HttpResponse = client.get("https://api.imgflip.com/get_memes")
            println(response.status)

            val data = response.body<MemeResponse>()

            client.close()
        }
    }
}